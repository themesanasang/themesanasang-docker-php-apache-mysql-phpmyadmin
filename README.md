ThemeSanasang: PHP/Apache/MySQL/PHPMyadmin
===================================

PHP_VERSION=5.6.30
APACHE_VERSION=2.4.32
MYSQL_VERSION=5.7

```
/php-apache-mysql-phpmyadmin/
├── apache
│   ├── Dockerfile
│   └── demo.apache.conf
├── docker-compose.yml
├── php
│   └── Dockerfile
├── data
│   └── mysql
└── public_html
    └── index.php
```
